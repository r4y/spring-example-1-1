package com.rayspringexample;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyApp {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ICoach myCoach = context.getBean("mySecondCoach", ICoach.class);
		System.out.println(myCoach.getTraining());
		context.close();
	}
}
